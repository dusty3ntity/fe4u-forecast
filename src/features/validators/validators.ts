export const fullTrim = (_: string, originalValue: string): string => {
	return originalValue.replace(/\s+/g, ' ').trim();
};
