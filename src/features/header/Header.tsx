import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useCookies } from 'react-cookie';

import { Button, UserIcon } from '../../components';
import { useUserContext } from '../../contexts';

interface INavProps {
	id: string;
	label: string;
	url: string;
}

const navElements: INavProps[] = [
	{
		id: 'forecast-link',
		label: 'Forecast',
		url: '/forecast',
	},
];

export const Header: React.FC = () => {
	const { user, setUser } = useUserContext();
	const [_, setCookie] = useCookies(['username']);

	const handleLogout = () => {
		setUser(undefined);
		setCookie('username', null);
	};

	return (
		<div className="header">
			<Link className="logo" to="/">
				<h1 className="logo-text">Forecast App</h1>
			</Link>

			<div className="nav">
				{navElements.map((item) => (
					<NavLink to={item.url} key={item.id} exact>
						<span className="nav-item" id={item.id}>
							{item.label}
						</span>
					</NavLink>
				))}
			</div>

			<div className="user-area">
				{user && (
					<>
						<UserIcon />
						<span className="name">{user.displayName}</span>
						<span className="divider" />
						<Button text="Log out" className="logout-btn" onClick={handleLogout} />
					</>
				)}
				{!user && (
					<Link to="/login" className="link">
						Log in
					</Link>
				)}
			</div>
		</div>
	);
};
