import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { LoadingScreen } from '../../components';
import { useUserContext } from '../../contexts';

export const PrivateRoute: React.FC<RouteProps> = ({ children, ...props }) => {
	const { user, loading } = useUserContext();

	if (loading === 'idle' || loading === 'pending') {
		return <LoadingScreen size={2} />;
	}

	return <Route {...props} render={() => (user ? children : <Redirect to={'/login'} />)} />;
};
