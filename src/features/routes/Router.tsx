import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { HomePage, NotFoundPage, ForecastPage, LoginPage } from '../../screens';
import { PrivateRoute } from './PrivateRoute';

export const Router: React.FC = () => {
	return (
		<Switch>
			<Route exact path="/" component={HomePage} />
			<Route exact path="/login" component={LoginPage} />

			<PrivateRoute exact path="/forecast">
				<ForecastPage />
			</PrivateRoute>

			<Route path="/not-found" component={NotFoundPage} />

			<Route>
				<Redirect to="/not-found" />
			</Route>
		</Switch>
	);
};
