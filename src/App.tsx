import { ToastContainer } from 'react-toastify';

import { Container } from './components';
import { UserProvider } from './contexts';
import { Header, Router } from './features';

export const App = () => {
	return (
		<div className="app">
			<ToastContainer
				position="bottom-right"
				limit={3}
				draggable={false}
				hideProgressBar
				closeOnClick={false}
				autoClose={5000}
			/>

			<UserProvider>
				<Container>
					<Header />

					<Router />
				</Container>
			</UserProvider>
		</div>
	);
};
