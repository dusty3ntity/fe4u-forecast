import React from 'react';
import { ErrorMessage } from '@hookform/error-message';

import { ErrorIcon } from '../icons';

export interface IValidationMessageProps {
	inputName: string;
	errors: any;
}

export const ValidationMessage: React.FC<IValidationMessageProps> = ({ errors, inputName }) => {
	return (
		<ErrorMessage
			errors={errors}
			name={inputName}
			render={({ message }: { message: string }) => (
				<div className="validation-error">
					{message}
					<ErrorIcon />
				</div>
			)}
		/>
	);
};
