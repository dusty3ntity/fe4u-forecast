import React from 'react';

import { IComponentProps } from '../../models/types';
import { combineClassNames } from '../../utils/classNames';

export const CheckIcon: React.FC<IComponentProps> = ({ id, className, ...props }) => {
	return (
		<svg id={id} className={combineClassNames('icon check-icon', className)} viewBox="0 0 24 24" {...props}>
			<path d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" />
		</svg>
	);
};
