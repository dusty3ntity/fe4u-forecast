import React from 'react';

import { IComponentProps } from '../../models';
import { combineClassNames } from '../../utils';
import { ErrorIcon, InfoIcon, SuccessIcon, WarningIcon } from '../../components';

export interface INotificationProps extends IComponentProps {
	type: NotificationType;
	title?: string;
	message?: string;
}

export enum NotificationType {
	Info = 'info',
	Success = 'success',
	Warning = 'warning',
	Error = 'error',
	UnknownError = 'unknown-error',
}

export const Notification: React.FC<INotificationProps> = ({ id, className, type, title, message }) => {
	if (!title) {
		switch (type) {
			case NotificationType.Info:
				title = 'Information';
				break;
			case NotificationType.Success:
				title = 'Success!';
				break;
			case NotificationType.Warning:
				title = 'Warning!';
				break;
			case NotificationType.Error:
				title = 'Error!';
				break;
			case NotificationType.UnknownError:
				title = 'Unknown error!';
				break;
		}
	}

	if (type === NotificationType.UnknownError) {
		message = 'An unknown error occurred... Please, refresh the page or contact the administrator.';
	}

	return (
		<div id={id} className={combineClassNames('notification', type, className)}>
			<div className="icon-container">
				{type === NotificationType.Info && <InfoIcon />}
				{type === NotificationType.Success && <SuccessIcon />}
				{type === NotificationType.Warning && <WarningIcon />}
				{(type === NotificationType.Error || type === NotificationType.UnknownError) && <ErrorIcon />}
			</div>

			<div className="content-container">
				<div className="title-container">
					<span className="title">{title}</span>
				</div>
				<div className="message">{message}</div>
			</div>
		</div>
	);
};
