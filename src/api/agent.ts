import axios, { AxiosResponse } from 'axios';

import { ForecastDataT } from '../models';
import { SLEEP_DURATION } from './../constants/api';

axios.interceptors.response.use(undefined, (error) => {
	if (process.env.REACT_APP_ENV === 'DEVELOPMENT') {
		console.error('API error:', error.response);
	}

	if (error.message === 'Network Error' && !error.response) {
		console.error('Network error', error);
		throw new Error('No connection');
	} else if (error.response.status === 500) {
		console.error('Server error', error);
		throw new Error('Server error');
	}
});

const responseBody = (response: AxiosResponse): any => response.data;

const sleep =
	() =>
	(response: AxiosResponse): Promise<AxiosResponse> => {
		return new Promise<AxiosResponse>((resolve) => {
			process.env.REACT_APP_ENV === 'DEVELOPMENT'
				? setTimeout(() => resolve(response), SLEEP_DURATION)
				: resolve(response);
		});
	};

const requests = {
	get: (url: string): Promise<any> => axios.get(url).then(sleep()).then(responseBody),
	post: (url: string, body?: {}): Promise<any> => axios.post(url, body).then(sleep()).then(responseBody),
	put: (url: string, body: {}): Promise<any> => axios.put(url, body).then(sleep()).then(responseBody),
	del: (url: string): Promise<any> => axios.delete(url).then(sleep()).then(responseBody),
};

export const Forecast = {
	get: (woeid: number): Promise<ForecastDataT> =>
		requests.get(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}`),
	getWoeid: (location: { lat: number; lng: number }): Promise<any> =>
		requests.get(
			`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/search/?lattlong=${location.lat},${location.lng}`
		),
};
