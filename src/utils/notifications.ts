import React from 'react';
import { toast } from 'react-toastify';

import { IComponentProps } from '../models';
import { Notification } from '../components';
import { NotificationType } from '../components/notifications/Notification';

export interface INotificationOptionsProps extends IComponentProps {
	title?: string;
	message?: string;
	error?: any;
	errorOrigin?: string;
}

export const createNotification = (type: NotificationType, options?: INotificationOptionsProps) => {
	toast(
		React.createElement(Notification, {
			type: type,
			...options,
		})
	);
};
