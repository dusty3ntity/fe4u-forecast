import moment from 'moment';

export const getRelativeFormattedDate = (date: string): string => {
	const parsedDate = moment(date);

	if (parsedDate.isSame(new Date(), 'day')) {
		return "Today";
	}

	if (parsedDate.isSame(moment().add(1, "day"), "day")) {
		return "Tomorrow";
	}

	return parsedDate.format("ddd D MMM");
};
