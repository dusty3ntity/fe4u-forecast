import { PageTitle } from '../../features/';

export const HomePage = () => {
	return (
		<div id="home-page">
			<PageTitle title="Home" />

			<h1>Forecast App</h1>
			<h2>By Vadym Ohyr</h2>
			<h3>2021</h3>
		</div>
	);
};
