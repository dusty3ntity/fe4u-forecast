import React from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useCookies } from 'react-cookie';

import { Button, Page, ValidationMessage } from '../../components';
import { PageTitle } from '../../features';
import { LoginUser } from '../../models';
import { combineClassNames, createNotification } from '../../utils';
import { history } from '../../config';
import { useUserContext } from '../../contexts';
import { users } from '../../config/users';
import { NotificationType } from '../../components/notifications/Notification';

export const LoginPage: React.FC = () => {
	const { setUser } = useUserContext();
	const [_, setCookie] = useCookies(['username']);

	const validationSchema: Yup.SchemaOf<LoginUser> = Yup.object().shape({
		username: Yup.string().required('Username is required.'),
		password: Yup.string().required('Password is required.'),
	});

	const {
		register,
		handleSubmit,
		formState: { errors, isDirty, submitCount, isValid },
	} = useForm<LoginUser>({ resolver: yupResolver(validationSchema) });

	const onSubmit = (credentials: LoginUser) => {
		const user = users.find((u) => u.username === credentials.username && u.password === credentials.password);
		if (user) {
			setUser(user);
			setCookie('username', user.username);
			history.push('/forecast');
			return;
		}

		createNotification(NotificationType.Error, {
			title: 'Authentication error!',
			message: 'The data provided is incorrect. Check your credentials and try again.',
		});
	};

	return (
		<>
			<PageTitle title="Log in" />

			<Page id="login-page">
				<h2>Log in</h2>

				<form id="login-form" className="form" onSubmit={handleSubmit(onSubmit)}>
					<div className="username-input form-item">
						<label htmlFor="username">
							<span>Username</span>
							<ValidationMessage inputName="username" errors={errors} />
						</label>

						<input
							id="username"
							className={combineClassNames('text-input', { error: errors.username })}
							type="text"
							autoFocus
							autoComplete="username"
							maxLength={20}
							{...register('username')}
						/>
					</div>

					<div className="displayName-input form-item">
						<label htmlFor="password">
							<span>Password</span>
							<ValidationMessage inputName="password" errors={errors} />
						</label>

						<input
							id="password"
							className={combineClassNames('text-input', { error: errors.password })}
							type="password"
							autoComplete="current-password"
							maxLength={30}
							{...register('password')}
						/>
					</div>

					<div className="actions-container">
						<Button
							className="login-btn"
							text="Log in"
							type="submit"
							disabled={!isDirty || (submitCount > 0 && !isValid)}
						/>
					</div>
				</form>
			</Page>
		</>
	);
};
