import { useEffect, useState } from 'react';

import { LoadingScreen, Page } from '../../components';
import { PageTitle } from '../../features';
import { Forecast } from '../../api/agent';
import { ForecastDataT } from '../../models';
import { createNotification } from '../../utils';
import { NotificationType } from '../../components/notifications/Notification';
import { ForecastDayList } from './ForecastDayList';

const LONDON_LOCATION = { lat: 51.5072, lng: 0.1276 };

const getLocation = async () => {
	const position: any = await new Promise((resolve, reject) => {
		navigator.geolocation.getCurrentPosition(resolve, reject);
	});

	const lat = position.coords.latitude;
	const lng = position.coords.longitude;
	return { lat, lng };
};

export const ForecastPage = () => {
	const [forecast, setForecast] = useState<ForecastDataT | undefined>();
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		const getForecast = async (location: { lat: number; lng: number }) => {
			try {
				setLoading(true);
				const woeid = (await Forecast.getWoeid(location))[0].woeid;
				const forecast = await Forecast.get(woeid);
				setForecast(forecast);
			} catch {
				createNotification(NotificationType.UnknownError);
			} finally {
				setLoading(false);
			}
		};

		const getForecastByLocationOrDefault = async () => {
			try {
				const location = await getLocation();
				await getForecast(location);
			} catch {
				createNotification(NotificationType.Error, {
					message: 'There was an error getting your location. Proceeding with the default one.',
				});
				await getForecast(LONDON_LOCATION);
			}
		};

		getForecastByLocationOrDefault();
	}, []);

	return (
		<>
			<PageTitle title="Forecast" />

			<Page id="forecast-page">
				{loading && <LoadingScreen size={2} />}

				{!loading && forecast && <ForecastDayList forecast={forecast} />}
			</Page>
		</>
	);
};
