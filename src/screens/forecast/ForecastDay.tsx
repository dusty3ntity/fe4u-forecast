import React from 'react';

import { ForecastDayDataT } from '../../models';
import { getRelativeFormattedDate } from '../../utils';

export interface IForecastDayProps {
	forecast: ForecastDayDataT;
}

export const ForecastDay: React.FC<IForecastDayProps> = ({ forecast }) => {
	const {
		weather_state_abbr,
		weather_state_name,
		wind_direction,
		wind_direction_compass,
		applicable_date,
		min_temp,
		max_temp,
		wind_speed,
		air_pressure,
		humidity,
		visibility,
		predictability,
	} = forecast;

	return (
		<div className="forecast-day">
			<span className="date">
				{getRelativeFormattedDate(applicable_date)}
			</span>
			<div className="data-container weather-container">
				<img
					src={`https://www.metaweather.com/static/img/weather/png/${weather_state_abbr}.png`}
					alt={weather_state_name}
					className="weather-icon"
				/>
				<span className="weather-type">{weather_state_name}</span>
			</div>
			<div className="data-container temperature-container">
				<span className="max-temperature">Max: {Math.ceil(max_temp)}°C</span>
				<span className="min-temperature">Min: {Math.ceil(min_temp)}°C</span>
			</div>
			<div className="wind-container">
				<span
					aria-label={`Wind direction: ${wind_direction_compass}`}
					className="wind-direction-icon"
					style={{ transform: `rotate(${wind_direction}deg)` }}
				/>
				<span className="data-container wind-value">{Math.round(wind_speed)}mph</span>
			</div>
			<div className="data-container additional-info-container">
				<span className="title">Humidity</span>
				<span className="value">{humidity}%</span>
				<span className="title">Visibility</span>
				<span className="value">{Math.round(visibility * 100) / 100} miles</span>
				<span className="title">Pressure</span>
				<span className="value">{air_pressure}mb</span>
			</div>
			<div className="data-container confidence-container">
				<span className="title">Confidence</span>
				<span className="value">{predictability}%</span>
			</div>
		</div>
	);
};
