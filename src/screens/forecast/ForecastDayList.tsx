import moment from 'moment';
import React from 'react';

import { ForecastDataT } from '../../models';
import { ForecastDay } from './ForecastDay';

export interface IForecastPageComponentProps {
	forecast: ForecastDataT;
}

export const ForecastDayList: React.FC<IForecastPageComponentProps> = ({ forecast }) => {
	const {
		parent: { title: countryName },
		title,
		consolidated_weather,
		sun_rise,
		sun_set,
	} = forecast;

	return (
		<>
			<div className="top-panel">
				<h2>Forecast</h2>
			</div>

			<div className="forecast-container">
				<div className="location">
					<span className="country-name">{countryName}</span>
					<span className="city-name">{title}</span>
				</div>

				<ul className="forecast-list">
					{consolidated_weather.map((d) => (
						<li className="forecast-list-item" key={d.id}>
							<ForecastDay forecast={d} />
						</li>
					))}
				</ul>

				<div className="sunrise-sunset">
					<div className="sunrise">
						<span className="title">Sunrise:</span>
						<span className="value">{moment(sun_rise).format('h:mm a')}</span>
					</div>
					<div className="sunset">
						<span className="title">Sunset:</span>
						<span className="value">{moment(sun_set).format('h:mm a')}</span>
					</div>
				</div>
			</div>
		</>
	);
};
