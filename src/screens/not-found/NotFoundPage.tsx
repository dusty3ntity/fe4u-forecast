import React from 'react';
import { Link } from 'react-router-dom';

import { Page } from '../../components';
import { PageTitle } from '../../features';

export const NotFoundPage: React.FC = () => {
	return (
		<>
			<PageTitle title="Not Found" />

			<Page id="not-found-page">
				<div className="error-code">404</div>

				<div className="content">
					<div className="title">Nothing here...</div>

					<Link to="/" className="btn">
						Go home
					</Link>
				</div>
			</Page>
		</>
	);
};
