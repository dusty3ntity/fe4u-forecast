import React, { createContext, useContext, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { users } from '../config/users';

import { User } from '../models';

interface UserContextValue {
	user: User | undefined;
	setUser: React.Dispatch<React.SetStateAction<User | undefined>>;
	loading: "idle" | "pending" | "done";
}

export const UserContext = createContext<UserContextValue | undefined>(undefined);

export const useUserContext = (): UserContextValue => {
	const c = useContext(UserContext);

	if (!c) {
		throw new Error('useUserContext must be inside UserProvider');
	}

	return c;
};

export const UserProvider: React.FC = ({ children }): JSX.Element => {
	const [user, setUser] = useState<User | undefined>();
	const [loading, setLoading] = useState<'idle' | 'pending' | 'done'>('idle');
	const [cookies, setCookie] = useCookies(['username']);

	useEffect(() => {
		setLoading('pending');
		if (cookies.username) {
			const user = users.find((u) => u.username === cookies.username);
			if (user) {
				setUser(user);
			} else {
				setCookie('username', null);
			}
		}
		setLoading('done');
	}, []);

	const value = { user, setUser, loading };

	return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};
