import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router';
import reportWebVitals from './reportWebVitals';

import { App } from './App';
import { history } from './config';
import { LoadingScreen } from './components';

import "react-toastify/dist/ReactToastify.min.css";
import './styles/styles.scss';

ReactDOM.render(
	<React.StrictMode>
		<Suspense fallback={<LoadingScreen size={8} />}>
			<Router history={history}>
				<App />
			</Router>
		</Suspense>
	</React.StrictMode>,
	document.getElementById('root')
);

reportWebVitals();
