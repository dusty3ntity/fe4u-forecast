export interface IComponentProps {
	id?: string;
	className?: string;
}
