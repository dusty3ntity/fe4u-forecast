export type User = {
	id: string;
	displayName: string;
	username: string;
	password: string;
};

export type LoginUser = {
	username: string;
	password: string;
};
