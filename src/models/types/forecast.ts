export type ForecastDataT = {
	time: string;
	sun_rise: string;
	sun_set: string;
	timezone_name: string;
	parent: {
		title: string;
	};
	title: string;
	consolidated_weather: ForecastDayDataT[];
};

export type ForecastDayDataT = {
	id: number;
	weather_state_name: string;
	weather_state_abbr: WeatherStateAbbrT;
	wind_direction_compass: WindDirectionCompassT;
	created: string;
	applicable_date: string;
	min_temp: number;
	max_temp: number;
	the_temp: number;
	wind_speed: number;
	wind_direction: number;
	air_pressure: number;
	humidity: number;
	visibility: number;
	predictability: number;
};

export type WeatherStateAbbrT = 'sn' | 'sl' | 'h' | 't' | 'hr' | 'lr' | 's' | 'hc' | 'lc' | 'c';
export type WindDirectionCompassT =
	| 'N'
	| 'NNE'
	| 'NE'
	| 'ENE'
	| 'E'
	| 'ESE'
	| 'SE'
	| 'SSE'
	| 'S'
	| 'SSW'
	| 'SW'
	| 'WSW'
	| 'W'
	| 'WNW'
	| 'NW'
	| 'NNW';
